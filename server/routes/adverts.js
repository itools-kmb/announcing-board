import { Router } from 'express';
import {
  getAdverts,
  getAdvertsById,
  createAdvert,
  updateAdvert,
  deleteAdvert,
} from '../controllers/adverts';

const router = Router();

router.get('/', getAdverts);
router.get('/:id', getAdvertsById);
router.post('/', createAdvert);
router.put('/:id', updateAdvert);
router.delete('/:id', deleteAdvert);

export default router;
