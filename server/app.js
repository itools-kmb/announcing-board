import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import usersRoute from './routes/users';
import advertsRoute from './routes/adverts';
import dao from './dao/index';

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use('/api/users', usersRoute);
app.use('/api/adverts', advertsRoute);

dao.connect()
  .then(() => {
    app.listen(3000, () => {
      console.log('Example app listening on port 3000!');
    });
  })
  .catch((err) => {
    throw new Error(err);
  });

// dao.init();
// .then(() => {
//   })
//     .catch((err) => {
//       console.log(err);
//     });
// })
// .catch((err) => {
//   console.log(err);
