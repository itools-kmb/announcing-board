const usersTableCreateQuery = 'CREATE TABLE users (\n'
  + 'ID SERIAL PRIMARY KEY,\n'
  + 'username VARCHAR(30),\n'
  + 'firstName VARCHAR(30),  \n'
  + 'lastName VARCHAR(30),\n'
  + 'email VARCHAR(30),\n'
  + 'password VARCHAR(30),\n'
  + 'phone integer\n'
  + ');';

const advertsTableCreateQuery = 'CREATE TABLE adverts (\n'
  + 'ID SERIAL PRIMARY KEY,\n'
  + 'title VARCHAR(30),\n'
  + 'created DATE,\n'
  + 'modified DATE,\n'
  + 'description VARCHAR(30),\n'
  + 'category VARCHAR(30),\n'
  + 'price integer,\n'
  + 'views integer,\n'
  + 'user_id integer REFERENCES users (id)\n'
  + ');';

export default {
  usersTableCreateQuery,
  advertsTableCreateQuery,
};
