import pool from '../dbConfig';

class Advert {
  constructor({
    title, description, category, price, views, userId,
  }) {
    this.title = title;
    this.description = description;
    this.category = category;
    this.price = price;
    this.views = views;
    this.userId = userId;
  }

  save() {
    return new Promise((resolve) => {
      const {
        title, description, category, price, views, userId,
      } = this;
      pool.query('INSERT INTO adverts (title, description, category, price, views, user_id) VALUES ($1, $2, $3, $4, $5, $6)',
        [title, description, category, price, views, userId], (error, results) => {
          if (error) throw new Error(error);
          resolve(`Advert added with ID: ${results}`);
        });
    });
  }

  static findAll() {
    return new Promise((resolve) => {
      pool.query('SELECT * FROM adverts ORDER BY id ASC', (error, results) => {
        if (error) throw new Error(error);
        resolve(results.rows);
      });
    });
  }

  static findOneById(id) {
    return new Promise((resolve) => {
      pool.query('SELECT * FROM adverts WHERE id = $1', [id], (error, results) => {
        if (error) throw new Error(error);
        resolve(results.rows);
      });
    });
  }

  static findByIdAndUpdate(id, { userId }) {
    return new Promise((resolve) => {
      pool.query(
        'UPDATE adverts SET user_id = $2 WHERE id = $1 ',
        [id, userId],
        (error) => {
          if (error) throw new Error(error);
          resolve(`Advert modified with ID: ${id}`);
        },
      );
    });
  }

  static findByIdAndRemove(id) {
    return new Promise((resolve) => {
      pool.query('DELETE FROM adverts WHERE id = $1', [id], (error) => {
        if (error) throw new Error(error);
        resolve(`Advert deleted with ID: ${id}`);
      });
    });
  }
}

export default Advert;
