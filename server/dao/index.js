import { Pool } from 'pg';
import dbConfig from '../dbConfig';
import sql from '../models';

const pool = new Pool(dbConfig);

class DB {
  query(text, params) {
    return new Promise((resolve) => {
      pool.query(text, params, (err, res) => {
        if (err) throw new Error(err);
        resolve(res);
      });
    });
  }
}


class DAO {
  constructor() {
    this.connection = this.connect();
  }

  connect() {
    return new Promise((resolve => {
      pool.connect((err, client, release) => {
        if (err) {
          return console.error('Error acquiring client', err.stack);
        }
        client.query('SELECT NOW()', (error) => {
          release();
          if (error) {
            return console.error('Error executing query', err.stack);
          }
        });
        resolve()
      });
    }));
  }


  init() {
    return new Promise((resolve) => {
      pool.query(sql.usersTableCreateQuery, (error) => {
        if (error) throw new Error(error);
        pool.query(sql.advertsTableCreateQuery, (err) => {
          if (err) throw new Error(error);
          console.log('DB was init!');
          resolve();
        });
      });
    });
  }

  clear() {
    return new Promise((resolve) => {
      pool.query('DROP TABLE IF EXISTS users, adverts CASCADE;', (error) => {
        if (error) throw new Error(error);
        console.log('DB was Dropped');
        resolve();
      });
    });
  }
}

const dao = new DAO();


export default dao;
