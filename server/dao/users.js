import pool from '../dbConfig';

class User {
  constructor({
    username, firstName, lastName, email, phone, password,
  }) {
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.password = password;
  }

  save() {
    return new Promise((resolve, reject) => {
      const {
        username, firstName, lastName, email, phone, password,
      } = this;
      console.log(username)
      pool.query(
        'INSERT INTO users (username, firstName, lastName, email, phone, password) VALUES ($1, $2, $3, $4, $5, $6)',
        [username, firstName, lastName, email, phone, password], (error, results) => {
          if (error) reject(error);
          resolve(`User added with ID: ${results}`);
        },
      );
    });
  }

  static findAll() {
    return new Promise((resolve) => {
      console.log('getAllUsers');
      pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
        if (error) throw new Error(error);
        resolve(results.rows);
      });
    });
  }

  static findOneById(id) {
    return new Promise((resolve) => {
      pool.query('SELECT * FROM users INNER JOIN adverts ON $1=adverts.user_id', [id], (error, results) => {
        if (error) throw new Error(error);
        resolve(results.rows);
      });
    });
  }

  static findByIdAndUpdate(id, updatedUser) {
    return new Promise((resolve) => {
      pool.query(
        'UPDATE users SET email = $1, name = $2 WHERE id = $3',
        [...updatedUser],
        (error) => {
          if (error) throw new Error(error);
          resolve(`User modified with ID: ${id}`);
        },
      );
    });
  }

  static findByIdAndRemove(id) {
    return new Promise((resolve) => {
      pool.query('DELETE FROM users WHERE id = $1', [id], (error) => {
        if (error) throw new Error(error);
        resolve(`User deleted with ID: ${id}`);
      });
    });
  }
}

export default User;
