import Advert from '../dao/adverts';

const createAdvert = (req, res) => {
  const advert = new Advert(req.body);

  advert.save()
    .then((msg) => {
      res
        .status(201)
        .json(msg);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const getAdverts = (req, res) => {
  Advert.findAll()
    .then((users) => {
      res
        .status(200)
        .json(users);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const getAdvertsById = (req, res) => {
  const id = parseInt(req.params.id, 10);
  Advert.findOneById(id)
    .then((user) => {
      res
        .status(200)
        .json(user);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const updateAdvert = (req, res) => {
  const id = parseInt(req.params.id, 10);
  const { userId, ...updatedParams } = req.body;
  const formattedUserId = parseInt(userId, 10);

  Advert.findByIdAndUpdate({ id, formattedUserId, ...updatedParams })
    .then((msg) => {
      res
        .status(200)
        .json(msg);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const deleteAdvert = (req, res) => {
  const id = parseInt(req.params.id, 10);
  Advert.findByIdAndRemove(id)
    .then((msg) => {
      res
        .status(200)
        .json(msg);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

export {
  getAdverts,
  getAdvertsById,
  createAdvert,
  updateAdvert,
  deleteAdvert,
};
