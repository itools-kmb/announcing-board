import User from '../dao/users';

const createUser = (req, res) => {
  const user = new User(req.body);

  user.save()
    .then((msg) => {
      res
        .status(201)
        .json(msg);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const getUsers = (req, res) => {
  User.findAll()
    .then((users) => {
      res
        .status(200)
        .json(users);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const getUserById = (req, res) => {
  const id = parseInt(req.params.id, 10);
  User.findOneById(id)
    .then((user) => {
      res
        .status(200)
        .json(user);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const updateUser = (req, res) => {
  const id = parseInt(req.params.id, 10);
  const user = new User(req.body);

  User.findByIdAndUpdate(id, user)
    .then((msg) => {
      res
        .status(200)
        .json(msg);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

const deleteUser = (req, res) => {
  const id = parseInt(req.params.id, 10);
  User.findByIdAndRemove(id)
    .then((msg) => {
      res
        .status(200)
        .json(msg);
    })
    .catch((error) => {
      res
        .status(400)
        .send({ message: `Error: ${error}` });
    });
};

export {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
